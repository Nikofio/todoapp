package ngf.uninsubria.it;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    final ListView listView = findViewById(R.id.list_view);
    final EditText editText = findViewById(R.id.input_insert_todo);
    final Button addButton = findViewById(R.id.button_add);

    final ArrayList<TodoItem> todoItems = new ArrayList<TodoItem>();

    final ArrayAdapter<TodoItem> adapter = new ArrayAdapter<TodoItem>(this,
            android.R.layout.simple_list_item_1, todoItems);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView.setAdapter(adapter);

        addButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String todo = editText.getText().toString();
        if (todo.length()==0) {
            Toast.makeText(getApplicationContext(),
                    "Empty ToDo string",
                    Toast.LENGTH_LONG).show();
            return;
        }
        TodoItem newTodo = new TodoItem(todo);
        todoItems.add(0, newTodo);
        adapter.notifyDataSetChanged();

        editText.setText("");

        InputMethodManager imm = (InputMethodManager) getSystemService(
                MainActivity.this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);
        editText.clearFocus();
    }
}
